#include "stdafx.h"
#include "GSEGame.h"
#include "math.h"

// GSEGame 클래스 생성자
GSEGame::GSEGame(int windowSizeX, int windowSizeY)
{
	// 렌더러
	m_Renderer = new Renderer(windowSizeX, windowSizeY);

	// 오브젝트 관리자
	m_ObjectMgr = new GSEObjectMgr;

	// TYPE_HERO 오브젝트 생성
	m_heroID = m_ObjectMgr->AddObject(0, 0, 0,
		30, 30, 1.f,
		1,
		0, 0, 0,
		0, 0, 0,
		0, 0, 0,
		TYPE_HERO,
		2000,
		-1);

	// 테스트용 TYPE_DEFAULT 오브젝트 생성
	for (int i = 0; i < 50; i++)
	{
		float x = (500.f * (float)rand() / (float)RAND_MAX) - 250.f;
		float y = (500.f * (float)rand() / (float)RAND_MAX) - 250.f;
		float z = 0.f;

		float sX = 50.f * (float)rand() / (float)RAND_MAX;
		float sY = 50.f * (float)rand() / (float)RAND_MAX;
		float sZ = 1.f; //  50.f * (float)rand() / (float)RAND_MAX;

		float mass = 100.f;

		float velX = (10.f * (float)rand() / (float)RAND_MAX) - 5.f;
		float velY = (10.f * (float)rand() / (float)RAND_MAX) - 5.f;
		float velZ = 0.f;

		float accX = 0.f;
		float accY = 0.f;
		float accZ = 0.f;

		float forceX = 0.f;
		float forceY = 0.f;
		float forceZ = 0.f;

		// 오브젝트 생성
		int id = m_ObjectMgr->AddObject(x, y, z,
			sX, sY, sZ,
			mass,
			velX, velY, velZ,
			accX, accY, accZ,
			forceX, forceY, forceZ,
			TYPE_DEFAULT,
			100,
			-1);
	}
}

// GSEGame 클래스 소멸자
GSEGame::~GSEGame()
{
	// 렌더러 삭제 및 초기화
	delete m_Renderer;
	m_Renderer = NULL;

	// 오브젝트 관리자 삭제 및 초기화
	delete m_ObjectMgr;
	m_ObjectMgr = NULL;
}

// DrawAll() 함수
void GSEGame::DrawAll(float elapsedTime)
{
	// ObjectMgr에서 작동
	if (m_Renderer != NULL)
	{
		m_ObjectMgr->UpdateAllObjects(elapsedTime); // 모든 오브젝트 업데이트
		m_ObjectMgr->DoGarbageCollect();
		m_ObjectMgr->DoAllObjectsOverlapTest();
		m_ObjectMgr->DrawAllObjects(m_Renderer, elapsedTime);    // 모든 오브젝트 그리기
	}

	m_gameTime += elapsedTime;
}

void GSEGame::KeyInput(GSEUserInterface* ui, float elapsedTime)
{
	// 캐릭터(hero) 동작

	float x, y, z;
	x = y = z = 0.f;

	float forceAmount = 800.f;

	if (ui->Is_SP_Arrow_Up_Down())
	{
		y += forceAmount;
	}
	if (ui->Is_SP_Arrow_Down_Down())
	{
		y -= forceAmount;
	}
	if (ui->Is_SP_Arrow_Left_Down())
	{
		x -= forceAmount;
	}
	if (ui->Is_SP_Arrow_Right_Down())
	{
		x += forceAmount;
	}
	m_ObjectMgr->AddObjectForce(m_heroID, x, y, z, elapsedTime); // 가속도 증가

	// 스페이스바가 눌린 경우에만 총알을 발사한다
	if (ui->Is_Spacebar_Down())
	{
		// 총알(bullet)

		// (시작 전) 총알을 쏠 수 있는가? (쿨타임이 다 지났는가?)
		bool canShoot = m_ObjectMgr->IsCoolTimeExpired(m_heroID);

		// 총알을 쏠 수 있다면
		if (canShoot)
		{
			// 1. 히어로 위치 구하기
			float x, y, z;
			float vx, vy, vz;
			m_ObjectMgr->GetObjectPos(m_heroID, &x, &y, &z);
			m_ObjectMgr->GetObjectVel(m_heroID, &vx, &vy, &vz);

			// 2. 총알 속도 구하기
			float mag = sqrtf(vx * vx + vy * vy);
			float bulletVX = 1.f;
			float bulletVY = 0.f;
			float bulletSpeed = 1000.f;

			float theta = m_gameTime * 10.f;
			float newBX = cos(theta) * bulletVX - sin(theta) * bulletVY;
			float newBY = sin(theta) * bulletVX + cos(theta) * bulletVY;

			newBX *= bulletSpeed;
			newBY *= bulletSpeed;

			// 3. 오브젝트 더하기 (속도, 위치)
			m_ObjectMgr->AddObject(x, y, z,
				5, 5, 10,
				1,
				newBX, newBY, 0,
				0, 0, 0,
				0, 0, 0,
				TYPE_BULLET,
				10,
				m_heroID);

			// 4. 쿨타임 초기화
			m_ObjectMgr->ResetCoolTime(m_heroID);
		}
	}
}
