#pragma once

#include "GSEGlobal.h"
#include "Renderer.h"
#include "GSEObjectMgr.h"
#include "GSEUserInterface.h"

class GSEGame
{

public:

	GSEGame(int windowSizeX, int windowSizeY); // GSEGame 클래스 생성자
	~GSEGame(); // GSEGame 클래스 소멸자

	void DrawAll(float elapsedTime); // DrawAll() 함수 
	void KeyInput(GSEUserInterface* ui, float elapsedTime);

private:
	Renderer* m_Renderer;
	GSEObjectMgr* m_ObjectMgr;

	int m_heroID = -1;

	float m_gameTime = 0.f;
};