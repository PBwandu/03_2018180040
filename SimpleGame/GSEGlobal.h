#pragma once

// 최대 오브젝트 개수
#define MAX_NUM_OBJECT 100000

// 중력 가속도
#define GRAVITY 9.8

// 타입 정의
#define TYPE_DEFAULT -1
#define TYPE_HERO 0
#define TYPE_BUILDING 1
#define TYPE_BULLET 2

// tbd