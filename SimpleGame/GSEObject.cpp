#include "stdafx.h"
#include "GSEObject.h"

GSEObject::GSEObject()
{
	m_sizeX = 0.f; // 크기
	m_sizeY = 0.f; // 크기
	m_sizeZ = 0.f; // 크기

	m_posX = 0.f; // 위치
	m_posY = 0.f; // 위치
	m_posZ = 0.f; // 위치

	m_mass = 0.f; // 질량

	m_velX = 0.f; // 속도
	m_velY = 0.f; // 속도
	m_velZ = 0.f; // 속도

	m_accX = 0.f; // 가속도
	m_accY = 0.f; // 가속도
	m_accZ = 0.f; // 가속도

	m_forceX = 0.f; // 외부 힘
	m_forceY = 0.f; // 외부 힘
	m_forceZ = 0.f; // 외부 힘

	m_type = TYPE_DEFAULT; // 종류

	m_cooltime = 0.01f;            // 쿨타임
	m_remainCooltime = m_cooltime; // 남은 쿨타임
	m_isCoolTimeExpired = false;   // 쿨타임 만료됨

	m_r = m_g = m_b = m_a = 1.f; // 색상
}

GSEObject::~GSEObject()
{
}

void GSEObject::SetSize(float x, float y, float z)
{
	m_sizeX = x;
	m_sizeY = y;
	m_sizeZ = z;
}

void GSEObject::SetPos(float x, float y, float z)
{
	m_posX = x;
	m_posY = y;
	m_posZ = z;
}

void GSEObject::GetPos(float* x, float* y, float* z)
{
	*x = m_posX;
	*y = m_posY;
	*z = m_posZ;
}

void GSEObject::SetMass(float mass)
{
	m_mass = mass;
}

float GSEObject::GetMass()
{
	return m_mass;
}

void GSEObject::SetVel(float x, float y, float z)
{
	m_velX = x;
	m_velY = y;
	m_velZ = z;
}

void GSEObject::GetVel(float* x, float* y, float* z)
{
	*x = m_velX;
	*y = m_velY;
	*z = m_velZ;
}

float GSEObject::GetVelMag()
{
	float mag = sqrtf(m_velX * m_velX + m_velY * m_velY + m_velZ * m_velZ);
	return mag;
}

void GSEObject::SetAcc(float x, float y, float z)
{
	m_accX = x;
	m_accY = y;
	m_accZ = z;
}

void GSEObject::SetForce(float x, float y, float z)
{
	m_forceX = x;
	m_forceY = y;
	m_forceZ = z;
}

void GSEObject::SetType(int type)
{
	m_type = type;
}

int GSEObject::GetType()
{
	return m_type;
}

void GSEObject::SetColor(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
}

void GSEObject::GetBBMin(float* x, float* y, float* z)
{
	*x = m_posX - m_sizeX / 2.f;
	*y = m_posY - m_sizeY / 2.f;
	*z = m_posZ - m_sizeZ / 2.f;
}

void GSEObject::GetBBMax(float* x, float* y, float* z)
{
	*x = m_posX + m_sizeX / 2.f;
	*y = m_posY + m_sizeY / 2.f;
	*z = m_posZ + m_sizeZ / 2.f;
}

void GSEObject::SetID(int id)
{
	m_ID = id;
}

int GSEObject::GetID()
{
	return m_ID;
}

void GSEObject::SetParent(int id)
{
	m_parentID = id;
}

bool GSEObject::isAncestor(int id)
{
	if (m_parentID == id) 
		return true;
	return false;
}

void GSEObject::SetHP(float hp)
{
	m_HP = hp;
}

float GSEObject::GetHP()
{
	return m_HP;
}

void GSEObject::SetCoolTime(float cooltime)
{
	m_cooltime = cooltime;
}

float GSEObject::GetCoolTime()
{
	return m_cooltime;
}

bool GSEObject::IsCoolTimeExpired()
{
	return m_isCoolTimeExpired;
}

void GSEObject::ResetCoolTime()
{
	m_remainCooltime = m_cooltime;
}

void GSEObject::Draw(Renderer* renderer)
{
	renderer->DrawSolidRect(m_posX, m_posY, m_posZ, m_sizeX, 
		m_r, m_g, m_b, m_a);
}

void GSEObject::Update(float elapsedTime)
{
	float t = elapsedTime;
	float tt = t * t;

	// 가속도
	// m_accX = m_forceX / m_mass; 
	// m_accY = m_forceY / m_mass;
	// m_accZ = m_forceZ / m_mass;

	// 속도
	// m_velX = m_velX + (m_accX * t);
	// m_velY = m_velY + (m_accY * t);
	// m_velZ = m_velZ + (m_accZ * t);

	// 위치
	// m_posX = m_posX + (m_velX * t) + (0.5f * m_accX * tt);
	// m_posY = m_posY + (m_velY * t) + (0.5f * m_accY * tt);
	// m_posZ = m_posZ + (m_velZ * t) + (0.5f * m_accZ * tt);

	// m_velX... 등등에 friction(마찰력) 적용

	// 일반 힘 불러오기
	float normalForce = m_mass * GRAVITY;

	// 마찰계수
	float frictionCoef = 20.f;

	// 마찰력
	float friction = frictionCoef * normalForce;

	float frictionDirX = -m_velX;
	float frictionDirY = -m_velY;

	float mag = sqrtf(frictionDirX * frictionDirX + frictionDirY * frictionDirY);

	if (mag > FLT_EPSILON)
	{
		frictionDirX = frictionDirX / mag;
		frictionDirY = frictionDirY / mag;

		float frictionForceX = frictionDirX * friction;
		float frictionForceY = frictionDirY * friction;

		float frictionAccX = frictionForceX / m_mass;
		float frictionAccY = frictionForceY / m_mass;

		// 가속도 결과
		float resultVelX = m_velX + frictionAccX * elapsedTime;
		float resultVelY = m_velY + frictionAccY * elapsedTime;
		float resultVelZ = m_velZ;

		if (resultVelX * m_velX < 0.f)
		{
			m_velX = 0.f;
		}
		else
		{
			m_velX = resultVelX;
		}
		if (resultVelY * m_velY < 0.f)
		{
			m_velY = 0.f;
		}
		else
		{
			m_velY = resultVelY;
		}
		if (resultVelZ * m_velZ < 0.f)
		{
			m_velZ = 0.f;
		}
		else
		{
			m_velZ = resultVelZ;
		}
	}

	// 위치
	m_posX = m_posX + (m_velX * t);
	m_posY = m_posY + (m_velY * t);
	m_posZ = m_posZ + (m_velZ * t);

	// 쿨타임 감소
	m_remainCooltime -= elapsedTime;

	// 쿨타임이 전부 다 지났을 경우
	if (m_remainCooltime < FLT_EPSILON)
	{
		m_isCoolTimeExpired = true;
	}
	else
	{
		m_isCoolTimeExpired = false;
	}
}

void GSEObject::AddForce(float x, float y, float z, float elapsedtime)
{
	// 가속도 계산
	float accX = x / m_mass;
	float accY = y / m_mass;
	float accZ = z / m_mass;

	m_velX = m_velX + accX * elapsedtime;
	m_velY = m_velY + accY * elapsedtime;
	m_velZ = m_velZ + accZ * elapsedtime;
}
