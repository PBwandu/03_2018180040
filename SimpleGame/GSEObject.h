#pragma once

#include "GSEGlobal.h"
#include "Renderer.h"

class GSEObject
{
public:
	GSEObject();
	~GSEObject();

	void SetSize(float x, float y, float z);
	void SetPos(float x, float y, float z);
	void GetPos(float* x, float* y, float* z);
	void SetMass(float mass);
	float GetMass();
	void SetVel(float x, float y, float z);
	void GetVel(float* x, float* y, float* z);
	float GetVelMag();
	void SetAcc(float x, float y, float z);
	void SetForce(float x, float y, float z);
	void SetType(int type);
	int GetType();
	void SetColor(float r, float g, float b, float a);

	void GetBBMin(float* x, float* y, float* z);
	void GetBBMax(float* x, float* y, float* z);

	void SetID(int id);
	int GetID();
	void SetParent(int id);
	bool isAncestor(int id);

	void SetHP(float hp);
	float GetHP();

	void SetCoolTime(float cooltime);
	float GetCoolTime();
	bool IsCoolTimeExpired();
	void ResetCoolTime();

	void Draw(Renderer* renderer);

	void Update(float elapsedTime);

	void AddForce(float x, float y, float z, float elapsedtime);

private:
	float m_sizeX, m_sizeY, m_sizeZ = 0.f;    // 크기
	float m_posX, m_posY, m_posZ = 0.f;       // 위치
	float m_mass = 0.f;                       // 질량
	float m_velX, m_velY, m_velZ = 0.f;       // 속도
	float m_accX, m_accY, m_accZ = 0.f;       // 가속도
	float m_forceX, m_forceY, m_forceZ = 0.f; // 외부 힘

	float m_r, m_g, m_b, m_a; // 색상

	int m_type = TYPE_DEFAULT; // 종류

	float m_cooltime = 1.f;           // 쿨타임
	float m_remainCooltime = 1.f;     // 남은 쿨타임
	bool m_isCoolTimeExpired = false; // 쿨타임 만료됨

	int m_ID = -1; // 본인 ID
	int m_parentID = -1; // 본인을 생성한 대상의 ID

	float m_HP = 0;
};

