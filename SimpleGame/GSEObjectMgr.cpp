// Object Manager - 오브젝트들을 관리

#include "stdafx.h"
#include "GSEObjectMgr.h"
#include <iostream>

GSEObjectMgr::GSEObjectMgr()
{
	for (int i = 0; i < MAX_NUM_OBJECT; i++)
	{
		m_Objects[i] = NULL;
		m_ObjectsOverlap[i] = false;
	}
}

GSEObjectMgr::~GSEObjectMgr()
{
	for (int i = 0; i < MAX_NUM_OBJECT; i++)
	{
		// 오즈젝트가 있다면 
		if (m_Objects[i] != NULL)
		{
			// 삭제하고 초기화
			delete m_Objects[i];
			m_Objects[i] = NULL;
		}
	}
}

int GSEObjectMgr::AddObject(float posX, float posY, float posZ, 
							float sizeX, float sizeY, float sizeZ, 
							float mass, 
							float velX, float velY, float velZ, 
							float accX, float accY, float accZ, 
							float forceX, float forceY, float forceZ, 
							int type, 
							float HP, 
							float ancestor)
{
	int index = -1;

	// 빈 공간을 찾는다
	for (int i = 0; i < MAX_NUM_OBJECT; i++)
	{
		if (m_Objects[i] == NULL)
		{
			index = i;
			break;
		}
	}

	// 빈 공간이 있는 경우
	if (index >= 0) 
	{
		// 빈 공간에 새로운 오브젝트를 만든다
		m_Objects[index] = new GSEObject;
		m_Objects[index]->SetPos(posX, posY, posZ);
		m_Objects[index]->SetSize(sizeX, sizeY, sizeZ);
		m_Objects[index]->SetMass(mass);
		m_Objects[index]->SetVel(velX, velY, velZ);
		m_Objects[index]->SetAcc(accX, accY, accZ);
		m_Objects[index]->SetForce(forceX, forceY, forceZ);
		m_Objects[index]->SetType(type);
		m_Objects[index]->SetID(index);
		m_Objects[index]->SetHP(HP);
		m_Objects[index]->SetParent(ancestor);

		return index;
	}
	
	// 빈 공간이 없는 경우
	std::cout << "No more empty object slot!" << std::endl;
	return index;
}

void GSEObjectMgr::SetObjectVel(int id, float vx, float vy, float vz)
{
	if (m_Objects[id] != NULL)
	{
		m_Objects[id]->SetVel(vx, vy, vz);
	}
	else
	{
		// 해당 id 자리에 오브젝트가 없음
	}
}

void GSEObjectMgr::GetObjectVel(int id, float* vx, float* vy, float* vz)
{
	if (m_Objects[id] != NULL)
	{
		m_Objects[id]->GetVel(vx, vy, vz);
	}
	else
	{
		// 해당 id 자리에 오브젝트가 없음
	}
}

void GSEObjectMgr::GetObjectPos(int id, float* x, float* y, float* z)
{
	if (m_Objects[id] != NULL)
	{
		m_Objects[id]->GetPos(x, y, z);
	}
	else
	{
		// 해당 id 자리에 오브젝트가 없음
	}
}

void GSEObjectMgr::SetCoolTime(int id, float cooltime)
{
	if (m_Objects[id] != NULL)
	{
		m_Objects[id]->SetCoolTime(cooltime);
	}
	else
	{
		// 해당 id 자리에 오브젝트가 없음
	}
}

float GSEObjectMgr::GetCoolTime(int id)
{
	if (m_Objects[id] != NULL)
	{
		return m_Objects[id]->GetCoolTime();
	}
	else
	{
		// 해당 id 자리에 오브젝트가 없음
		return 0.f;
	}
}

bool GSEObjectMgr::IsCoolTimeExpired(int id)
{
	if (m_Objects[id] != NULL)
	{
		return m_Objects[id]->IsCoolTimeExpired();
	}
	else
	{
		// 해당 id 자리에 오브젝트가 없음
		return false;
	}
}

void GSEObjectMgr::ResetCoolTime(int id)
{
	if (m_Objects[id] != NULL)
	{
		return m_Objects[id]->ResetCoolTime();
	}
	else
	{
		// 해당 id 자리에 오브젝트가 없음
	}
}

void GSEObjectMgr::AddObjectForce(int id, float x, float y, float z, float elapsedtime)
{
	if (m_Objects[id] != NULL)
	{
		m_Objects[id]->AddForce(x, y, z, elapsedtime);
	}
	else
	{
		// 해당 id 자리에 오브젝트가 없음
	}
}

bool GSEObjectMgr::DeleteObject(int id)
{
	// 해당 id의 오브젝트가 있을 경우
	if (m_Objects[id] != NULL) {
		// 오브젝트를 지우고 초기화한다
		delete m_Objects[id];
		m_Objects[id] = NULL;
		return true;
	}

	std::cout << "index " << id << " does not exists" << std::endl;
	return false;
}

void GSEObjectMgr::UpdateAllObjects(float elapsedTime)
{
	for (int i = 0; i < MAX_NUM_OBJECT; i++)
	{
		if (m_Objects[i] != NULL)
		{
			m_Objects[i]->Update(elapsedTime);
		}
	}
}

void GSEObjectMgr::DoGarbageCollect()
{
	// garbage를 찾아서 삭제한다
	for (int i = 0; i < MAX_NUM_OBJECT;i++)
	{
		if (m_Objects[i] != NULL)
		{
			int type = m_Objects[i]->GetType();

			// 총알 Garbage
			if (type == TYPE_BULLET)
			{
				float mag = m_Objects[i]->GetVelMag();

				if (mag < FLT_EPSILON)
				{
					// 오브젝트 삭제
					DeleteObject(i);
					continue;
				}
			}

			// 체력이
			float hp = m_Objects[i]->GetHP();
			if (hp < FLT_EPSILON)
			{
				// 오브젝트 삭제
				DeleteObject(i);
				continue;
			}
		}
	}
}

void GSEObjectMgr::DoAllObjectsOverlapTest()
{
	memset(m_ObjectsOverlap, 0, sizeof(bool) * MAX_NUM_OBJECT);

	for (int i = 0; i < MAX_NUM_OBJECT; i++)
	{
		// 오브젝트가 없으면 루프 종료
		if (m_Objects[i] == NULL) break; 

		for (int j = i + 1; j < MAX_NUM_OBJECT; j++)
		{
			// 오브젝트가 없으면 루프 종료
			if (m_Objects[j] == NULL) break;

			// 두 오브젝트간 겹침(충돌) 확인
			bool isOverlap = BBOverlap(i, j);

			if (isOverlap)
			{
				bool ancestorA = m_Objects[i]->isAncestor(j);
				bool ancestorB = m_Objects[j]->isAncestor(i);

				// 서로가 독립된(생성되거나 생성한 관계가 아닌) 오브젝트일 때
				if (ancestorA == false && ancestorB == false)
				{
					m_ObjectsOverlap[i] = true;
					m_ObjectsOverlap[j] = true;

					// 충돌 처리
					CollisionProcessing(i, j);

					// 체력 변동
					float iHP = m_Objects[i]->GetHP();
					float jHP = m_Objects[j]->GetHP();
					float fiHP = iHP - jHP;
					float fjHP = jHP - iHP;
					m_Objects[i]->SetHP(fiHP);
					m_Objects[j]->SetHP(fjHP);
				}
			}
		}
	}

	// 겹침
	for (int i = 0; i < MAX_NUM_OBJECT; i++)
	{
		if (m_Objects[i] != NULL)
		{
			if (m_ObjectsOverlap[i])
			{
				m_Objects[i]->SetColor(1, 0, 0, 1);
			}
			else
			{
				m_Objects[i]->SetColor(1, 1, 1, 1);
			}
		}
	}
}

bool GSEObjectMgr::BBOverlap(int srcID, int dstID)
{
	if ( (m_Objects[srcID] != NULL) && (m_Objects[dstID] != NULL) ) 
	{
		GSEObject* src = m_Objects[srcID];
		GSEObject* dst = m_Objects[dstID];

		float srcMinX, srcMinY, srcMinZ, srcMaxX, srcMaxY, srcMaxZ;
		float dstMinX, dstMinY, dstMinZ, dstMaxX, dstMaxY, dstMaxZ;

		src->GetBBMin(&srcMinX, &srcMinY, &srcMinZ);
		src->GetBBMax(&srcMaxX, &srcMaxY, &srcMaxZ);
		dst->GetBBMin(&dstMinX, &dstMinY, &dstMinZ);
		dst->GetBBMax(&dstMaxX, &dstMaxY, &dstMaxZ);

		// 충돌 아닌 조건들
		if (srcMinX > dstMaxX) return false;
		if (srcMaxX < dstMinX) return false;
		if (srcMinY > dstMaxY) return false;
		if (srcMaxY < dstMinY) return false;
		if (srcMinZ > dstMaxZ) return false;
		if (srcMaxZ < dstMinZ) return false;

		// 충돌 true
		return true;
	}
	return false;
}

void GSEObjectMgr::CollisionProcessing(int srcID, int dstID)
{
	GSEObject* src = m_Objects[srcID];
	GSEObject* dst = m_Objects[dstID];

	float srcMass = src->GetMass();
	float dstMass = dst->GetMass();

	float srcVx, srcVy, srcVz;
	src->GetVel(&srcVx, &srcVy, &srcVz);
	float dstVx, dstVy, dstVz;
	dst->GetVel(&dstVx, &dstVy, &dstVz);

	float srcFVx, srcFVy, srcFVz;
	srcFVx = (((srcMass - dstMass) / (srcMass + dstMass)) * srcVx +
		((2 * dstMass) / (srcMass + dstMass)) * dstVx);
	srcFVy = (((srcMass - dstMass) / (srcMass + dstMass)) * srcVy +
		((2 * dstMass) / (srcMass + dstMass)) * dstVy);
	srcFVz = (((srcMass - dstMass) / (srcMass + dstMass)) * srcVz +
		((2 * dstMass) / (srcMass + dstMass)) * dstVz);

	float dstFVx, dstFVy, dstFVz;
	dstFVx = (((2 * srcMass) / (srcMass + dstMass)) * srcVx +
		((dstMass - srcMass) / (srcMass + dstMass)) * dstVx);
	dstFVy = (((2 * srcMass) / (srcMass + dstMass)) * srcVy +
		((dstMass - srcMass) / (srcMass + dstMass)) * dstVy);
	dstFVz = (((2 * srcMass) / (srcMass + dstMass)) * srcVz +
		((dstMass - srcMass) / (srcMass + dstMass)) * dstVz);

	src->SetVel(srcFVx, srcFVy, srcFVz);
	dst->SetVel(dstFVx, dstFVy, dstFVz);
}

void GSEObjectMgr::DrawAllObjects(Renderer* renderer, float elapsedTime)
{
	for (int i = 0; i < MAX_NUM_OBJECT; i++)
	{
		if (m_Objects[i] != NULL)
		{
			m_Objects[i]->Draw(renderer);
		}
	}
}
