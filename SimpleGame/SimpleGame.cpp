/*
Copyright 2022 Lee Taek Hee (Tech University of Korea)

This program is free software: you can redistribute it and/or modify
it under the terms of the What The Hell License. Do it plz.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY.
*/

#include "stdafx.h"
#include <iostream>
#include "Dependencies\glew.h"
#include "Dependencies\freeglut.h"
#include "timeapi.h";

#include "GSEGlobal.h"
#include "GSEGame.h" // "Renderer.h" include 포함
#include "GSEUserInterface.h"

GSEGame* g_game = NULL;
GSEUserInterface* g_userinterface = NULL;

DWORD g_startTime = 0;
DWORD g_prevTime = 0;

// 왼도우 기본값

const int   WINPOS[2] = { 700, 250 };             // 윈도우 시작 위치
const int   WINSIZE[2] = { 500, 500 };             // 윈도우 크기
const float BGCOLOR[4] = { 0.f, 0.3f, 0.3f, 1.f }; // 배경 색상

// 렌더링 함수

void RenderScene(void)
{
	// 시간 확인
	DWORD elapsedTime = 0;
	DWORD currentTime = timeGetTime();
	if (g_prevTime == 0)
	{
		elapsedTime = currentTime - g_startTime;
	}
	else
	{
		elapsedTime = currentTime - g_prevTime;
	}
	g_prevTime = currentTime;


	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(BGCOLOR[0], BGCOLOR[1], BGCOLOR[2], BGCOLOR[3]);
	timeGetTime();

	// elapsedTime을 ms에서 s로 변환
	float elapsedTimeSec = (float)elapsedTime / 1000;

	// 키 입력 전송
	g_game->KeyInput(g_userinterface, elapsedTimeSec);

	// GSEGame::DrawAll(elapsedTime) 함수 호출
	g_game->DrawAll(elapsedTimeSec);

	glutSwapBuffers();
}

void Idle(void)
{
	// printf("Idle 함수 호출됨\n"); 
	// 아무것도 안 하고 있을 때 호출됨

	RenderScene(); // RenderScene 함수 호출
}

void MouseInput(int button, int state, int x, int y)
{
	// printf("MouseInput 함수 호출됨\n");
	// 마우스 누르거나 뗄 시 호출됨 (즉, 클릭 한 번에 2번 호출됨)

	RenderScene(); // RenderScene 함수 호출
}

void KeyDownInput(unsigned char key, int x, int y)
{
	int id = -1;
	g_userinterface->KeyDown(key, id);
}

void KeyUpInput(unsigned char key, int x, int y)
{
	int id = -1;
	g_userinterface->KeyUp(key, id);
}

void SpecialKeyDownInput(int id, int x, int y)
{
	unsigned char key = '~';
	g_userinterface->KeyDown(key, id);
}

void SpecialKeyUpInput(int id, int x, int y)
{
	unsigned char key = '~';
	g_userinterface->KeyUp(key, id);
}


int main(int argc, char** argv)
{
	// Initialize GL things
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA); // 화면 표시 모드
	glutInitWindowPosition(WINPOS[0], WINPOS[1]);              // 윈도우 시작 위치
	glutInitWindowSize(WINSIZE[0], WINSIZE[1]);                // 윈도우 크기
	glutCreateWindow("Game Software Engineering TUKorea");     // 윈도우 생성 및 제목

	glewInit();
	if (glewIsSupported("GL_VERSION_3_0")) { std::cout << " GLEW Version is 3.0\n "; }
	else { std::cout << "GLEW 3.0 not supported\n "; }

	// Renderer 초기화
	g_game = new GSEGame(WINSIZE[0], WINSIZE[1]);
	g_userinterface = new GSEUserInterface();

	glutDisplayFunc(RenderScene);
	glutIdleFunc(Idle);
	glutKeyboardFunc(KeyDownInput);
	glutKeyboardUpFunc(KeyUpInput);
	glutMouseFunc(MouseInput);
	glutSpecialFunc(SpecialKeyDownInput);
	glutSpecialUpFunc(SpecialKeyUpInput);

	g_startTime = timeGetTime();

	glutMainLoop();

	delete g_game;

	return 0;
}

